/******************************************************************************
 * 
 * Copyright 2024, Black Sesame Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef PARASE_JSON_H
#define PARASE_JSON_H

#include <vector>
//nlohmann json
#include "json.hpp"

#include "iostream"
#include "mutex"

 using Json = nlohmann::json;

namespace hanhai {
namespace tool {
struct FullData {
  std::string app_path;
  std::string info;
};

class ParaseJson {
 public:
  explicit ParaseJson();
  ~ParaseJson();
  std::vector<FullData> full_data_vec;
  std::vector<FullData> ReadJsonFile(const std::string& file_name);

 private:
  std::mutex parase_json_lock;
  std::vector<std::string> root_node_name;
  std::vector<FullData> ParaseJsonObj(const Json &root);
};

}  // namespace tool
}  // namespace hanhai

#endif
