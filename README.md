# tool_entry用法说明


### 修改配置文件

工具中配置文件/etc/hanhai/conf/tools_entry/tools_entry_conf.json

此配置文件记录了target端所有的应用工具;如客户自己有开发的小工具也可按照此配置文件格式写入文本;
其中"path"为工具的二进制文件参存放地址，
"info"为工具的功能介绍

```
"path": "/usr/bin/",
"info": "used to do ..."
```

### 启动

lt <option>

```
option: -h //显示此工具的帮助
option: -l //显示target端所有的工具地址及功能详情
```

###    **License**

The project is licensed under the [Apache-2.0] license.


