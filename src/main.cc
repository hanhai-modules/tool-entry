/******************************************************************************
 * 
 * Copyright 2024, Black Sesame Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include <string.h>

#include <iostream>

#include "../include/json_parase.h"

const std::string conf_path =
    "/etc/hanhai/conf/tools_entry/tools_entry_conf.json";
#define VERSION "0.1.0"

using namespace hanhai::tool;

class Command {
 public:
  int CommandType(std::string option) {
    const char* op;
    op = option.c_str();
    if (strcmp(op, "-l") == 0) {
      JudgeOption();
      return 0;
    } else if (strcmp(op, "-h") == 0) {
      PrintHelp();
      return 0;
    } else if (strcmp(op, "--version") == 0) {
      PrintVersion();
      return 0;
    } else {
      std::cerr << "Parameter is incorrect, please refer to :lt -h"
                << std::endl;
      return -1;
    }
  }

 private:
  int JudgeOption() {
    ParaseJson parase;
    auto vec = parase.ReadJsonFile(conf_path);
    int count = 1;
    if (vec.size() <= 0) {
      return -1;
    } else {
      std::cout << "This device has " << vec.size() << " hanhai tools installed"
                << "\n";
      for (int i = 0; i < vec.size(); ++i) {
        auto path = vec.at(i).app_path;
        auto info = vec.at(i).info;
        std::cout << "\n";
        std::cout << "Tool: " << path << "\n";
        std::cout << "Usage: " << info << "\n";
        ++count;
      }
    }
    return 0;
  }

  int PrintHelp() {
    std::cout << "Usage:\n"
              << "lt "
              << " [option]\nOption:\n"
              << "  -h print help info\n"
              << "  -l list all tools details\n"
              << std::endl;
    return 0;
  }

  void PrintVersion() {
    std::cout << "lt  " << VERSION << std::endl;
  }
};

int main(int argc, char* argv[]) {
  Command command;
  int num = -1;
  if (argc == 1) {
    std::string option = "-h";
    num = command.CommandType(option);
    return num;
  } else if (argc == 2) {
    std::string option = std::string(argv[1]);
    num = command.CommandType(option);
    return num;
  } else {
    num = command.CommandType("unknown");
    return num;
  }
  return 0;
}
