/******************************************************************************
 * 
 * Copyright 2024, Black Sesame Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#include "../include/json_parase.h"
#include "fstream"
#include "string"

namespace hanhai {
namespace tool {

ParaseJson::ParaseJson() {
}

ParaseJson::~ParaseJson() {
}

std::vector<FullData> ParaseJson::ReadJsonFile(const std::string& file_path) {
  std::ifstream fin(file_path);
  if(  !fin.is_open()){
    std::vector<FullData> data_null = {};
    return data_null;
  }
  Json root;
  fin >> root;
  fin.close();
auto   json = root["tools"];
  full_data_vec.clear();
  full_data_vec = ParaseJsonObj(json);
  return full_data_vec;
}

std::vector<FullData> ParaseJson::ParaseJsonObj(
    const Json &root) {
  std::vector<FullData> full_data_vector;

  for (const auto &it : root) {
    FullData data;
    data.app_path = it["path"];
    data.info = it["info"];
    full_data_vector.push_back(data);
  }

  return full_data_vector;
}

}  // namespace tool
}  // namespace hanhai